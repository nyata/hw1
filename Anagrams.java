/*
* 2014/06/12
* Anagrams
* Nagisa Yata
*/

import java.io.*;
import java.util.*;

public class Anagrams {

	private FileReader fr;
	private BufferedReader br;
	private String filePath = "/usr/share/dict/american-english";
	public ArrayList<Dictionary> dict;
	public ArrayList<Dictionary> dictDefault;

	public Anagrams() {
		dict = new ArrayList<Dictionary>();
		dictDefault = new ArrayList<Dictionary>();
	}

	public static void main(String[] args) {
		Anagrams an = new Anagrams();
		an.search(args[0]);
	}

	private void search(String arg) {
		String[] args = new String[arg.length()];

		for(int j = 0; j < arg.length(); j++) {
			char text;
			text = arg.charAt(j);
			args[j]  = String.valueOf(text);
		}

		//sort by alphabet
		Arrays.sort(args);

		arg = "";
		for(String a : args) {
			arg += a;
		}

		try {
			fr = new FileReader(filePath);
			br = new BufferedReader(fr);
			String line;

			int i = 0;
			//load dict file
			while((line = br.readLine()) != null) {
				//delete word which is longer than target word's length
				if(line.length() <= arg.length()) {
					//base dictionary
					dictDefault.add(new Dictionary(i, line));
					i++;
				}
			}

			// sort string length DESC
			Collections.sort(dictDefault, new CompareStr());

			for(Dictionary dictWord : dictDefault) {
				String[] word = new String[dictWord.getWord().length()];
				String line2 = "";//(a-z)*";

				for(int j = 0; j < dictWord.getWord().length(); j++) {
					char text;
					text = dictWord.getWord().charAt(j);
					word[j]  = String.valueOf(text);
				}

					//sort by alphabet
				Arrays.sort(word);

				//regular expression
				for(String w : word) {
					line2 += w + ".*";
				}

				//make new dictionary
				//dict.add(new Dictionary(i, line2));

				// search word
				if((arg.toLowerCase()).matches(line2.toLowerCase())) {
					System.out.println(dictWord.getWord());
					//break;
				}
			}

		} catch(IOException e) {
			System.out.println("error");
		} finally {
			try {
				br.close();
				fr.close();
			} catch(IOException e) {
				System.out.println("error2");
			}
		}

	}
}

class CompareStr implements Comparator {
	public int compare(Object o1, Object o2) {
		String w1 = ((Dictionary)o1).getWord();
		String w2 = ((Dictionary)o2).getWord();
		int n1 = w1.length();
		int n2 = w2.length();
		return n1 > n2 ? -1:1;
	}
}

class Dictionary {
	public int id;
	public String word;

	public Dictionary(int idx, String wordx) {
		this.id = idx;
		this.word = wordx;
	}

	public int getId() {
		return id;
	}

	public String getWord() {
		return word;
	}
}